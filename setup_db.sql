DROP DATABASE IF EXISTS Module_151;
CREATE DATABASE Module_151;
USE Module_151;

DROP TABLE IF EXISTS Person;
DROP TABLE IF EXISTS ProfessionDetail;
DROP TABLE IF EXISTS Department;

CREATE TABLE Department (
  ID INT UNIQUE AUTO_INCREMENT PRIMARY KEY UNIQUE,
  Name VARCHAR(255)
);

CREATE TABLE ProfessionDetail (
  ID INT AUTO_INCREMENT PRIMARY KEY UNIQUE ,
  uuid VARCHAR(255),
  Department_ID INT,
  Profession VARCHAR(255),
  Description VARCHAR(255),
  FOREIGN KEY (Department_ID) REFERENCES Department(ID)

);

CREATE TABLE Person (
  ID INT AUTO_INCREMENT PRIMARY KEY UNIQUE ,
  Firstname VARCHAR(255),
  Lastname VARCHAR(255),
  Birthdate DATE,
  AHV VARCHAR(14) UNIQUE ,
  EmployeeNumber VARCHAR(255) UNIQUE,
  Telephone VARCHAR(20),
  ProfessionDetail_ID INT,
  FOREIGN KEY (ProfessionDetail_ID) REFERENCES ProfessionDetail(ID)
);

INSERT INTO department (Name) VALUES ('marketing');
INSERT INTO department (Name) VALUES ('reception');

INSERT INTO professiondetail (Department_ID, Profession, Description) VALUES (1, 'Software engineer', 'Is jeffin some code');
INSERT INTO professiondetail (Department_ID, Profession, Description) VALUES (1, 'Business Jeff', 'Is jeffin some Business stuff');
INSERT INTO professiondetail (Department_ID, Profession, Description) VALUES (2, 'Software engineer', 'Is jeffin some code at reception');
INSERT INTO professiondetail (Department_ID, Profession, Description) VALUES (2, 'Business Jeff', 'Is jeffin some Business stuff at reception');

CREATE PROCEDURE store_data(_firstname nvarchar(255), _lastname varchar(255), _birthdate DATE, _AHV varchar(255), _employeeNumber varchar(255), _telephone varchar(255), _professionID INT)
BEGIN
  INSERT INTO person (Firstname, Lastname, Birthdate, AHV, EmployeeNumber, Telephone, ProfessionDetail_ID) VALUES (_firstname, _lastname, _birthdate, _AHV, _employeeNumber, _telephone, _professionID);
END;
