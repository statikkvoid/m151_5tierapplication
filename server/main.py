import flask
import json
from flask import jsonify

import utils

flsk = flask.Flask(__name__)

class Encoder(json.JSONEncoder):
    def _default(self, o):
        return o.__dict__

@flsk.route("/")
def default():
    flask.abort(403)

@flsk.route("/person",methods=["GET"])
def get_persons():
    if(not 'summoner_name' in flask.request.values):
        return "No summoner name given"


@flsk.route("/person", methods=["POST"])
def submit_person():
    pass