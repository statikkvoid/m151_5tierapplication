class Error(Exception):
    """Default error for this module"""


class ArgumentError(Error):
    """Invalid arguments were given"""

def check_params(dic, *args):
    """Check if certain arguments are given in a dict
    If an argument or another is required, but not both
    use the pipe oprator '|' to check them.

    Args:
        dict: Dictionary to check
        args: Keys to check
    Raises:
        ArgumentError: If a key does not exist
    """
    for arg in args:
        one_there = False
        for a in arg.split('|'):
            if a in dic:
                one_there = True
        if not one_there:
            raise ArgumentError
    return True